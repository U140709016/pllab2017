#!/usr/bin/perl
use strict;
use warnings;
# File: readname.pl
# Read in name and age

print "Enter your name: ";
my $name = <>; # "<>" reads one line from "standard input"
chomp $name; # chomp deletes any newlines from end of string

print "Enter your age: ";
my $age = <>;
chomp $age;

print "Hello, $name! ";
print "On your next birthday, you will be ", $age+1, ".\n";

my $p = 10 / 7;
printf "I have written %f programs.\n", $p;
printf "I have written %6.2f programs.\n", $p;
printf "I have written %0.2f programs.\n", $p;

my $x =25;
if ($x) { print "the value of \$x =$x\n";}
else {print "\$x isundefined or equal to 0 or empty string.\n";
}

my @a =("Ali");
if (@a) {
print "the length of \@a =".scalar@a."\n";}
else {print "\ @a isundefined or empty.\n";
}

my $str = "hello, world!";
$x = substr $str, 0, 8 ; 
print "$x\n";

substr $str, 7, 5,"$name";
print "$str\n";

my $dna = "AATATATAATGC";
my $pos = index $dna, "ATA";
print "ATG found at position $pos\n"; # answer: 1
pos = -1;
while (($pos = index($dna, "ATA", $pos)) > -1) {
print "ATG found at position $pos\n";
$pos++;
}


exit;